<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Zygmunt Krynicki <me@zygoon.pl>
-->

# Snapcraft packaging for Hare 

This repository contains snapcraft packaging for Hare language.
